import requests
import json

def search(card):
    url = "https://api.scryfall.com/cards/named"
    r = requests.get(url, params={"fuzzy": card})
    data = json.loads(r.text)

    if data["object"] != "card":
        print("fail")
    else:
        return data


def image(card):
    data = search(card)
    return data["image_uris"]["normal"]


def name(card):
    data = search(card)
    print(data["name"])
    return data["name"]
