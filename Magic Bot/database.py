import mysql.connector

mydb = None
mycursor =None

# Connects the program to the database
# run on startup
def connect():
    global mydb
    mydb = mysql.connector.connect(
        host="127.0.0.1",
        user="bot",
        passwd="BotSQL11!",
        database="legality"
    )
    global mycursor
    mycursor = mydb.cursor()


# Increments a card's # of times played,
# or adds it to the database if it hasn't
# been added yet
def played(name):
    sql = "SELECT plays FROM cards WHERE name=%s"
    n = (name,)

    mycursor.execute(sql, n)

    try:
        result = mycursor.fetchone()[0]

        new = result + 1

        sql = "UPDATE cards SET plays=%s WHERE name=%s"
        n = (new, name,)
    except TypeError:
        sql = "INSERT INTO cards (name, wins, plays) VALUES (%s, %s, %s)"
        n = (name, 0, 1)

    mycursor.execute(sql, n)


# Increments a commander's # of times played,
# or adds it to the database if it hasn't
# been added yet
def played_commander(name):
    sql = "SELECT plays FROM commanders WHERE name=%s"
    n = (name,)

    mycursor.execute(sql, n)

    try:
        result = mycursor.fetchone()[0]

        new = result + 1

        sql = "UPDATE commanders SET plays=%s WHERE name=%s"
        n = (new, name,)
    except TypeError:
        sql = "INSERT INTO commanders (name, wins, plays) VALUES (%s, %s, %s)"
        n = (name, 0, 1)

    mycursor.execute(sql, n)


# Increments a card's # of wins
def won(name):
    sql = "SELECT wins FROM cards WHERE name=%s"
    n = (name,)

    mycursor.execute(sql, n)

    result = mycursor.fetchone()[0]

    new = result + 1

    sql = "UPDATE cards SET wins=%s WHERE name=%s"
    n = (new, name,)

    mycursor.execute(sql, n)


# Increments a commander's # of wins
def won_commander(name):
    sql = "SELECT wins FROM commanders WHERE name=%s"
    n = (name,)

    mycursor.execute(sql, n)

    result = mycursor.fetchone()[0]

    new = result + 1

    sql = "UPDATE commanders SET wins=%s WHERE name=%s"
    n = (new, name,)

    mycursor.execute(sql, n)


def is_legal(name):
    sql = "SELECT wins FROM cards WHERE name=%s"
    n = (str(name),)

    mycursor.execute(sql, n)

    result = 0
    try:
        result = mycursor.fetchone()[0]
    except TypeError:
        None

    if result == 3:
        return "%s is banned" % (name,)
    else:
        return "%s is legal" % (name,)


def submit_decklist(decklist):
    for x in range(1, decklist.index("//play-1") - 1):
        played(decklist[x].split(" ", 1)[1])

    print(decklist[decklist.index("//play-1") + 1].split(" ", 1)[1])
    played(decklist[decklist.index("//play-1") + 1].split(" ", 1)[1])
    played_commander(decklist[decklist.index("//play-1") + 1].split(" ", 1)[1])

    mydb.commit()


def submit_winner(decklist):
    submit_decklist(decklist)

    for x in range(1, decklist.index("//play-1") - 1):
        won(decklist[x].split(" ", 1)[1])
    won(decklist[decklist.index("//play-1") + 1].split(" ", 1)[1])
    won_commander(decklist[decklist.index("//play-1") + 1].split(" ", 1)[1])

    mydb.commit()
